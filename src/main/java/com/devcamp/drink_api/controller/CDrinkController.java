package com.devcamp.drink_api.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drink_api.model.CDrink;
import com.devcamp.drink_api.repository.IDrinkRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRepository iDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks(){
        try {
            List<CDrink> listDrink = new ArrayList<>();
            iDrinkRepository.findAll().forEach(listDrink :: add);
            return new ResponseEntity<List<CDrink>>(listDrink, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id){
        try {
            CDrink dink = iDrinkRepository.findById(id);
            return new ResponseEntity<CDrink>(dink, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/drinks")
    public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pDrink){
        try {
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
            CDrink _drinks = iDrinkRepository.save(pDrink);
            return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++:::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to create specified Drink " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Object> updateDrink(@PathVariable("id") long id, @RequestBody CDrink pDrink){
        try {
            CDrink drinkData = iDrinkRepository.findById(id);
            CDrink drink = drinkData;
            drink.setMaNuocUong(pDrink.getMaNuocUong());
            drink.setTenNuocUong(pDrink.getTenNuocUong());
            drink.setDonGia(pDrink.getDonGia());
            drink.setGhiChu(pDrink.getGhiChu());
            drink.setNgayCapNhat(new Date());
            try {
                return new ResponseEntity<>(iDrinkRepository.save(drink), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity().body("Failed to update specified Drink " + e.getCause().getCause().getMessage());
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //xoá theo id
    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id){
        try {
            iDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //xoá all
    @DeleteMapping("/drinks")
    public ResponseEntity<CDrink> deleteALLDrink(){
        try {
            iDrinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
